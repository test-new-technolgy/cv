# 專案描述

### 詳細設計文件
https://drive.google.com/file/d/1RyIPMLr8m7oZ6FiZcYBlyX6D0Uuz4Lde/view?usp=sharing

### 主要有幾個步驟
|purpose          | .py |
--------------|:-----:|
|1.當年keras無法識別.jpeg故寫隻小程式將.jpeg轉成.jpg |picture_type_transform.py|
|2.前處理圖片(偏移/裁減)避免過度擬合|pre_process_picture.py|
|3.training deep learning model|train_model.py|
|4.transform text to mp3|text_to_mp3.py|
|5.用QT畫GUI當目標出現時撥放預先錄好的mp3|qt_app.py|
### 警告
* cascade屬於舊式機器學習方法現已被取代, please google yolov4
* reference to https://github.com/Hironsan/BossSensor
* 這個專案存在的目的只是給我紀念青春2017/1，已退AI圈數年，專案年久失修許多套件已無法下載